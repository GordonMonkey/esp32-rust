use esp_idf_hal::{gpio::{Level, Pin, PinDriver}, peripheral::Peripheral, prelude::Peripherals};
use esp_idf_svc::{eventloop::EspSystemEventLoop, nvs::{EspNvsPartition, NvsDefault, EspDefaultNvsPartition}, timer::{EspTimerService, Task, EspTaskTimerService}, wifi::{AsyncWifi, EspWifi}, ping::EspPing, http::server::EspHttpServer};
use esp_idf_sys as _;
use esp_idf_hal::gpio::OutputPin;
use std::{thread::sleep, time::Duration};
use std::env;
use dotenv::dotenv;
use wifi::Wifi;


pub const WIFI_SSID: &str = env!("WIFI_SSID");
pub const WIFI_PASS: &str = env!("WIFI_PASS");


mod wifi;


fn main() {
    esp_idf_sys::link_patches();
    esp_idf_svc::log::EspLogger::initialize_default();
    dotenv().ok();


    let peripherals = Peripherals::take().unwrap();
    let sysloop = EspSystemEventLoop::take().unwrap();
    let timer_service = EspTaskTimerService::new().unwrap();
    let _wifi = Wifi::connect(peripherals.modem, sysloop, Some(EspDefaultNvsPartition::take().unwrap()), timer_service).unwrap();

    let mut led = PinDriver::output(peripherals.pins.gpio2).unwrap();
    let button = PinDriver::input(peripherals.pins.gpio0).unwrap();

    loop{
        let button_state = button.get_level();
        match button_state {
            Level::Low => {
                led.set_high().unwrap();
            },
            Level::High => {
                led.set_low().unwrap();
            },
        }
        log::warn!("Button state: {:?}", button_state);
        
        sleep(Duration::from_millis(80));
    }
}

