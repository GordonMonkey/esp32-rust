use esp_idf_hal::peripheral::Peripheral;
use esp_idf_svc::{
    eventloop::EspSystemEventLoop, 
    nvs::{EspNvsPartition, NvsDefault}, 
    timer::{EspTimerService, Task}, 
    wifi::{AsyncWifi, EspWifi}, 
    ping::EspPing
};
use esp_idf_sys as _;
use esp_idf_svc::wifi::ClientConfiguration;
use esp_idf_svc::wifi::Configuration;
use esp_idf_svc::ping::Configuration as PingCfg;
use esp_idf_svc::wifi::AuthMethod;
use heapless::String;
use crate::{WIFI_PASS, WIFI_SSID};
use futures::executor::block_on;


pub struct Wifi;


impl Wifi {
    pub fn connect(
        modem: impl Peripheral<P = esp_idf_hal::modem::Modem> + 'static,
        sysloop: EspSystemEventLoop,
        nvs: Option<EspNvsPartition<NvsDefault>>,
        timer_service: EspTimerService<Task>,
    ) -> anyhow::Result<AsyncWifi<EspWifi<'static>>> {
        
        let mut wifi = AsyncWifi::wrap(
        EspWifi::new(modem, sysloop.clone(), nvs)?,
            sysloop,
            timer_service.clone(),
        )?;

        block_on(Self::establish_connection(&mut wifi))?;
        
        
        let ip_info = wifi.wifi().sta_netif().get_ip_info()?;
        log::info!("Wifi DHCP info: {:?}", ip_info);
        
        EspPing::default().ping(ip_info.subnet.gateway, &PingCfg::default())?;
        Ok(wifi)
    
    }
    

    async fn establish_connection(wifi: &mut AsyncWifi<EspWifi<'static>>) -> anyhow::Result<()> {
        let mut ssid: String<32> = String::new();
        let mut password: String<64> = String::new();
        
        ssid.push_str(WIFI_SSID).unwrap();
        password.push_str(WIFI_PASS).unwrap();
    
        wifi.set_configuration(&Configuration::Client(ClientConfiguration {
            ssid,
            bssid: None,
            auth_method: AuthMethod::WPA2Personal,
            password,
            channel: None,
        }))?;
    
        
       
        wifi.start().await?;
        log::info!("Wifi started");
        
        wifi.connect().await?;
        log::info!("Wifi connected");
    
        wifi.wait_netif_up().await?;
        log::info!("Wifi netif up");
        Ok(())
    }
}


